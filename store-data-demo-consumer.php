<?php

// time is saved by GMT 0
date_default_timezone_set('UTC');

// Time Array
$GLOBALS['time_array'][1]  ='00:00:00';
$GLOBALS['time_array'][2]  ='00:30:00';
$GLOBALS['time_array'][3]  ='01:00:00';
$GLOBALS['time_array'][4]  ='01:30:00';
$GLOBALS['time_array'][5]  ='02:00:00';
$GLOBALS['time_array'][6]  ='02:30:00';
$GLOBALS['time_array'][7]  ='03:00:00';
$GLOBALS['time_array'][8]  ='03:30:00';
$GLOBALS['time_array'][9]  ='04:00:00';
$GLOBALS['time_array'][10] ='04:30:00';
$GLOBALS['time_array'][11] ='05:00:00';
$GLOBALS['time_array'][12] ='05:30:00';
$GLOBALS['time_array'][13] ='06:00:00';
$GLOBALS['time_array'][14] ='06:30:00';
$GLOBALS['time_array'][15] ='07:00:00';
$GLOBALS['time_array'][16] ='07:30:00';
$GLOBALS['time_array'][17] ='08:00:00';
$GLOBALS['time_array'][18] ='08:30:00';
$GLOBALS['time_array'][19] ='09:00:00';
$GLOBALS['time_array'][20] ='09:30:00';
$GLOBALS['time_array'][21] ='10:00:00';
$GLOBALS['time_array'][22] ='10:30:00';
$GLOBALS['time_array'][23] ='11:00:00';
$GLOBALS['time_array'][24] ='11:30:00';
$GLOBALS['time_array'][25] ='12:00:00';
$GLOBALS['time_array'][26] ='12:30:00';
$GLOBALS['time_array'][27] ='13:00:00';
$GLOBALS['time_array'][28] ='13:30:00';
$GLOBALS['time_array'][29] ='14:00:00';
$GLOBALS['time_array'][30] ='14:30:00';
$GLOBALS['time_array'][31] ='15:00:00';
$GLOBALS['time_array'][32] ='15:30:00';
$GLOBALS['time_array'][33] ='16:00:00';
$GLOBALS['time_array'][34] ='16:30:00';
$GLOBALS['time_array'][35] ='17:00:00';
$GLOBALS['time_array'][36] ='17:30:00';
$GLOBALS['time_array'][37] ='18:00:00';
$GLOBALS['time_array'][38] ='18:30:00';
$GLOBALS['time_array'][39] ='19:00:00';
$GLOBALS['time_array'][40] ='19:30:00';
$GLOBALS['time_array'][41] ='20:00:00';
$GLOBALS['time_array'][42] ='20:30:00';
$GLOBALS['time_array'][43] ='21:00:00';
$GLOBALS['time_array'][44] ='21:30:00';
$GLOBALS['time_array'][45] ='22:00:00';
$GLOBALS['time_array'][46] ='22:30:00';
$GLOBALS['time_array'][47] ='23:00:00';
$GLOBALS['time_array'][48] ='23:30:00';


// ----------------------------------------------------------------------------------------------------------------------------------------------------------------


$pp_id            = 'e1ecaf3120d3bfd97a80277df0d4d4f69f178c76f9b010da489c8490bf43e1ec';

$user_consumption = 300;
$deviation        = rand(10, 30); // getting random deviation number range from 10 to 30

/*
*
*
* Insert Time in SGT Here
*
*
*/

$startTime        = '2019-02-26 00:00:00';
$now              = '2019-02-26 10:30:00';


// ================================================================================================================================================================
// =========================================================== DON'T EDIT ANYTHING FROM HERE AFTER ================================================================
// ================================================================================================================================================================


$nowTimeSlot    = explode(' ',$now);


// Getting No of Days
$date1          = date_create($startTime);
$date2          = date_create($now);
$diff           = date_diff($date1 , $date2);
$NoOfDays       = $diff->format("%a")+1; // adding 1 to adjust it

// Getting TimeArray Slot
for($i=1; $i <= 48; $i++) {

    // Check Array
    if($GLOBALS['time_array'][$i] == $nowTimeSlot[1]) {
        $timeFrameId = $i;
    }

}

$start_day      = date('d',strtotime($startTime)); 
$start_month    = date('m',strtotime($startTime)); 
$start_year     = date('Y',strtotime($startTime)); 



// Postgres Connection for AWS
$host             = "host   = 13.229.205.4";
$port             = "port   = 5432";
$credentials      = "user   = postgres password = electrify";

$dbname           = "dbname = postgres";
$dbSynergy        = "dbname = Ecosystem";

// connecting to cms database
$db  = pg_connect("$host $port $dbname $credentials");

if(!$db) {
    echo "Error : Unable to open database\n";
}

// connecting to Synergy database
$dbs = pg_connect("$host $port $dbSynergy $credentials");

if(!$dbs) {
    echo "Error : Unable to open database\n";
}


// ================================================================================================================================================================
// =========================================================== DON'T EDIT ANYTHING FROM HERE AFTER ================================================================
// ================================================================================================================================================================


function execStoreData($db,$dbs,$pp_id,$exactTime,$user_consumption,$deviation,$SRLP_value) {

    $MeterType          = 'C';
    $PPDateUTC          = date('Y-m-d H:i:s', strtotime($exactTime . "-8 hours"));

    $adjusted_value     = (($SRLP_value * $user_consumption) / 1000);
    $gen_val_per_day    = $adjusted_value / 30;
    //$gen_val_per_day  = $adjusted_value;
    $final_value        = round($gen_val_per_day + ($gen_val_per_day * (rand(0, $deviation) / 100)),4);


    $InsertData         = '     INSERT INTO 
                                    "PowerPod"."PowerPodReading"(
                                        "PPId",
                                        "PPReading", 
                                        "PPDate", 
                                        "UserTypeRID",
                                        "ReadingTypeRCode",
                                        "SlaveRID",
                                        "RegisterRID"
                                ) 
                                VALUES 
                                    (
                                        \''.$pp_id.'\',
                                        '.$final_value.', 
                                        \''.$PPDateUTC.'\', 
                                        1,
                                        \''.$MeterType.'\',
                                        1,
                                        0
                                    );';

    $Results = pg_query($dbs, $InsertData);
    if(!$Results) {
        echo pg_last_error($dbs);
    } 
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------


$SRLP        = 'SELECT "Period", "NormalizedValue" FROM "Marketplace"."RefSRLPInfo" ORDER BY "SRLPId"';
$SRLPResults = pg_query($dbs, $SRLP);

if(!$SRLPResults) {
   echo pg_last_error($dbs);
   exit;
}

while($row = pg_fetch_array($SRLPResults)) {

    $Period     = $row['Period'];
    $SRLP_value = $row['NormalizedValue'];

    $GLOBALS['SRLPArray'][$Period] = $SRLP_value;

}

// Loop through Days
for($j=1; $j <= $NoOfDays; $j++) {

    // adding leading zero
    $PropDate = sprintf("%02d", $start_day);

    if($now === date("$start_year-$start_month-$PropDate $time_array[$timeFrameId]")) {
        
        // Loop for Particular Period Only
        for($x=1; $x <= $timeFrameId; $x++) {

            $SRLP_value = $SRLPArray[$x];
            $exactTime  = date("$start_year-$start_month-$PropDate").' '.$time_array[$x];

            execStoreData($db,$dbs,$pp_id,$exactTime,$user_consumption,$deviation,$SRLP_value);

        }
    
    } else {

        // loop for 48 period
        for($k=1; $k <= 48; $k++) {
            
            $SRLP_value = $SRLPArray[$k];
            $exactTime  = date("$start_year-$start_month-$PropDate").' '.$time_array[$k];

            execStoreData($db,$dbs,$pp_id,$exactTime,$user_consumption,$deviation,$SRLP_value);
         
        }

    }

    $start_day = $start_day + 1;

}


echo 'done';

pg_close($db);
pg_close($dbs);



