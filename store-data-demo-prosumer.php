<?php

// this script is purely for consumers only !!!
// time is saved by GMT 0
date_default_timezone_set('UTC');


// ----------------------------------------------------------------------------------------------------------------------------------------------------------------

/*
*
*
* Insert Time in UTC Here
*
*
*/

$DemoPPId = '9e1ec829abffc3c8084465f62ddde8650f14e3a0b374d5135f71829581d9e1ec';
$FromDate = '2019-04-01 16:00:00';
$ToDate   = '2019-04-08 15:30:00';


// ================================================================================================================================================================
// =========================================================== DON'T EDIT ANYTHING FROM HERE AFTER ================================================================
// ================================================================================================================================================================


// Postgres Connection for AWS
$host1            = "host   = pgsql-electrify-2.ckw3aym3r7qp.ap-southeast-1.rds.amazonaws.com";
$host2            = "host   = 13.229.205.4";
//$host2            = "host   = pgsql-staging-synergy.ckw3aym3r7qp.ap-southeast-1.rds.amazonaws.com";

$port             = "port   = 5432";
$credentials      = "user   = postgres password = fWkb5Ff22UHwVkwv";
$credentials_demo = "user   = postgres password = electrify";
$dbname           = "dbname = Ecosystem";


$ConsQry    =  '
                SELECT 
                    "PPDate",
                    "PPId", 
                    "PPReading", 
                    "UserTypeRID",
                    "ReadingTypeRCode", 
                    "SlaveRID", 
                    "RegisterRID"
                FROM 
                    "PowerPod"."PowerPodReading"

                WHERE 
                    "PPId" IN (\'1c9093ad307269aff28e58c620b28fe5ba354464d47d620c983e371717606c9e\') AND
                    "PPDate" BETWEEN \''.$FromDate.'\' AND \''.$ToDate.'\'
                ORDER BY 
                    "PPDate"

                ';


$ProdQry    =  '
                SELECT 
                    "PPDate",
                    "PPId", 
                    "PPReading", 
                    "UserTypeRID",
                    "ReadingTypeRCode", 
                    "SlaveRID", 
                    "RegisterRID"
                FROM 
                    "PowerPod"."PowerPodReading"

                WHERE 
                    "PPId" IN (\'a53fc2c7a5444f12ae3d0908ea710942398f246672187ee62e05d2b56611b5c1\') AND
                    "ReadingTypeRCode" IN (\'P\') AND
                    "PPDate" BETWEEN \''.$FromDate.'\' AND \''.$ToDate.'\'

                ORDER BY 
                    "PPDate"

                ';


// connecting to database
$db  = pg_connect("$host1 $port $dbname $credentials");

if(!$db) {
    echo "Error : Unable to open database\n";
}


// connecting to Staging database
$dbs = pg_connect("$host2 $port $dbname $credentials_demo");
//$dbs = pg_connect("$host2 $port $dbname $credentials");

if(!$dbs) {
    echo "Error : Unable to open database\n";
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------


// Consumption Query
$ConsQryResults = pg_query($db, $ConsQry);

if(!$ConsQryResults) {
   echo pg_last_error($db);
   exit;
}

// looping ConsQryResults
while($row = pg_fetch_array($ConsQryResults)) {

    $ConsPPDate           = $row['PPDate'];
    $ConsPPId             = $row['PPId'];
    $ConsPPReading        = $row['PPReading'];
    $ConsUserTypeRID      = $row['UserTypeRID'];
    $ConsReadingTypeRCode = $row['ReadingTypeRCode'];
    $ConsSlaveRID         = $row['SlaveRID'];
    $ConsRegisterRID      = $row['RegisterRID'];


    if($ConsPPReading == NULL) {
        $ConsPPReading = 0;
    }

    $PPArray[$ConsPPDate]['C'][] = number_format($ConsPPReading, 4, '.', '');

    
    // Insert into PowerPod Readings in Staging

    $insertCons  =   '
                        INSERT INTO "PowerPod"."PowerPodReading" (
                                "PPId", 
                                "PPReading", 
                                "PPDate", 
                                "UserTypeRID", 
                                "DateAdded", 
                                "ReadingTypeRCode", 
                                "SlaveRID", 
                                "RegisterRID"
                        ) VALUES (
                            \''.$DemoPPId.'\', 
                            \''.$ConsPPReading.'\', 
                            \''.$ConsPPDate.'\', 
                                3, 
                            \''.date('Y-m-d H:i:s').'\', 
                                \'C\', 
                            \''.$ConsSlaveRID.'\', 
                            \''.$ConsRegisterRID.'\'
                        );
                    ';

    
    
    $insertRec = pg_query($dbs, $insertCons);

    if(!$insertRec) {
        echo pg_last_error($dbs);
    } 
    /*/**/
    //echo $insertCons."<br />";

}


// Production Query

$ProdQryResults = pg_query($db, $ProdQry);

if(!$ProdQryResults) {
   echo pg_last_error($db);
   exit;
}

// looping mainQry
while($row = pg_fetch_array($ProdQryResults)) {

    $ProdPPDate           = $row['PPDate'];
    $ProdPPId             = $row['PPId'];
    $ProdPPReading        = $row['PPReading'];
    $ProdUserTypeRID      = $row['UserTypeRID'];
    $ProdReadingTypeRCode = $row['ReadingTypeRCode'];
    $ProdSlaveRID         = $row['SlaveRID'];
    $ProdRegisterRID      = $row['RegisterRID'];


    if($ProdPPReading == NULL) {
        $ProdPPReading = 0;
    }
    

    $PPArray[$ProdPPDate]['P'][] = number_format(abs($ProdPPReading), 4, '.', '');


    $insertProd  =   '
                        INSERT INTO "PowerPod"."PowerPodReading" (
                                "PPId", 
                                "PPReading", 
                                "PPDate", 
                                "UserTypeRID", 
                                "DateAdded", 
                                "ReadingTypeRCode", 
                                "SlaveRID", 
                                "RegisterRID"
                        ) VALUES (
                            \''.$DemoPPId.'\', 
                            \''.$ProdPPReading.'\', 
                            \''.$ProdPPDate.'\', 
                                3, 
                            \''.date('Y-m-d H:i:s').'\', 
                                \'P\', 
                            \''.$ProdSlaveRID.'\', 
                            \''.$ProdRegisterRID.'\'
                        );
                    ';

    
    $insertRec = pg_query($dbs, $insertProd);

    if(!$insertRec) {
        echo pg_last_error($dbs);
    } 
    /**/

}



// Insert Nett Data

foreach($PPArray as $date=>$values) {

    $Cons = $values['C'][0];
    $Prod = $values['P'][0];
    
    $Nett = ($Cons - $Prod);
    $NettReading = number_format($Nett, 4, '.', '');


    $insertNett  =   '
                        INSERT INTO "PowerPod"."PowerPodReading" (
                                "PPId", 
                                "PPReading", 
                                "PPDate", 
                                "UserTypeRID", 
                                "DateAdded", 
                                "ReadingTypeRCode", 
                                "SlaveRID", 
                                "RegisterRID"
                        ) VALUES (
                            \''.$DemoPPId.'\', 
                            \''.$NettReading.'\', 
                            \''.$date.'\', 
                                3, 
                            \''.date('Y-m-d H:i:s').'\', 
                                \'N\', 
                            0, 
                            0
                        );
                    ';

    
    $insertRec = pg_query($dbs, $insertNett);

    if(!$insertRec) {
        echo pg_last_error($dbs);
    } 
    /**/


}



pg_close($db);
pg_close($dbs);