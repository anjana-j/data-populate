<?php

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------

// Postgres Connection for AWS
$host1            = "host   = pgsql-electrify-2.ckw3aym3r7qp.ap-southeast-1.rds.amazonaws.com";
$host2            = "host   = 13.229.205.4";

$port             = "port   = 5432";
$credentials      = "user   = postgres password = fWkb5Ff22UHwVkwv";
$credentials_demo = "user   = postgres password = electrify";

$dbname           = "dbname = postgres";
$dbSynergy        = "dbname = Ecosystem";

// connecting to cms database
$db  = pg_connect("$host1 $port $dbname $credentials");

if(!$db) {
    echo "Error : Unable to open database\n";
}

// connecting to Synergy database
$db_demo = pg_connect("$host2 $port $dbname $credentials_demo");

if(!$db_demo) {
    echo "Error : Unable to open database\n";
}


// ----------------------------------------------------------------------------------------------------------------------------------------------------------------

/*
*
* IMPORTANT : TIME IS IN SGT
*
*/



$mainQry =  "
                SELECT 
                    startdate, enddate, period, demand, usep, datetime
                FROM 
                    cms.tblmarketdata
                WHERE
                    startdate >= '2019-03-10'

                ORDER BY 
                    startdate  DESC
                
                OFFSET 1

            ";

// ================================================================================================================================================================
// =========================================================== DON'T EDIT ANYTHING FROM HERE AFTER ================================================================
// ================================================================================================================================================================

// startdate BETWEEN '2019-01-14 00:00:00' AND '2019-01-20 11:00:00'

// tapping mainQry
$mainQryResult = pg_query($db, $mainQry);

if(!$mainQryResult) {
   echo pg_last_error($db);
   exit;
}


// looping mainQry
while($row = pg_fetch_array($mainQryResult)) {

    $startdate = $row['startdate'];
    $enddate   = $row['enddate'];
    $period    = $row['period'];
    $demand    = $row['demand'];
    $usep      = $row['usep'];
    $datetime  = $row['datetime'];



    $demoQry    = '
                    INSERT INTO 
                        cms.tblmarketdata (
                            startdate, enddate, period, demand, usep, datetime
                    ) VALUES (
                        \''.$startdate.'\', 
                        \''.$enddate.'\', 
                        \''.$period.'\', 
                        \''.$demand.'\', 
                        \''.$usep.'\', 
                        \''.$datetime.'\'
                    );';

    $Insert = pg_query($db_demo, $demoQry);

    if(!$Insert) {
        echo pg_last_error($db_demo);
    }

}


pg_close($db);
pg_close($db_demo);