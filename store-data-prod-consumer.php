<?php

// time is saved by GMT 0
date_default_timezone_set('UTC');

// Time Array
$GLOBALS['time_array'][1]  ='00:00:00';
$GLOBALS['time_array'][2]  ='00:30:00';
$GLOBALS['time_array'][3]  ='01:00:00';
$GLOBALS['time_array'][4]  ='01:30:00';
$GLOBALS['time_array'][5]  ='02:00:00';
$GLOBALS['time_array'][6]  ='02:30:00';
$GLOBALS['time_array'][7]  ='03:00:00';
$GLOBALS['time_array'][8]  ='03:30:00';
$GLOBALS['time_array'][9]  ='04:00:00';
$GLOBALS['time_array'][10] ='04:30:00';
$GLOBALS['time_array'][11] ='05:00:00';
$GLOBALS['time_array'][12] ='05:30:00';
$GLOBALS['time_array'][13] ='06:00:00';
$GLOBALS['time_array'][14] ='06:30:00';
$GLOBALS['time_array'][15] ='07:00:00';
$GLOBALS['time_array'][16] ='07:30:00';
$GLOBALS['time_array'][17] ='08:00:00';
$GLOBALS['time_array'][18] ='08:30:00';
$GLOBALS['time_array'][19] ='09:00:00';
$GLOBALS['time_array'][20] ='09:30:00';
$GLOBALS['time_array'][21] ='10:00:00';
$GLOBALS['time_array'][22] ='10:30:00';
$GLOBALS['time_array'][23] ='11:00:00';
$GLOBALS['time_array'][24] ='11:30:00';
$GLOBALS['time_array'][25] ='12:00:00';
$GLOBALS['time_array'][26] ='12:30:00';
$GLOBALS['time_array'][27] ='13:00:00';
$GLOBALS['time_array'][28] ='13:30:00';
$GLOBALS['time_array'][29] ='14:00:00';
$GLOBALS['time_array'][30] ='14:30:00';
$GLOBALS['time_array'][31] ='15:00:00';
$GLOBALS['time_array'][32] ='15:30:00';
$GLOBALS['time_array'][33] ='16:00:00';
$GLOBALS['time_array'][34] ='16:30:00';
$GLOBALS['time_array'][35] ='17:00:00';
$GLOBALS['time_array'][36] ='17:30:00';
$GLOBALS['time_array'][37] ='18:00:00';
$GLOBALS['time_array'][38] ='18:30:00';
$GLOBALS['time_array'][39] ='19:00:00';
$GLOBALS['time_array'][40] ='19:30:00';
$GLOBALS['time_array'][41] ='20:00:00';
$GLOBALS['time_array'][42] ='20:30:00';
$GLOBALS['time_array'][43] ='21:00:00';
$GLOBALS['time_array'][44] ='21:30:00';
$GLOBALS['time_array'][45] ='22:00:00';
$GLOBALS['time_array'][46] ='22:30:00';
$GLOBALS['time_array'][47] ='23:00:00';
$GLOBALS['time_array'][48] ='23:30:00';

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------

/*
*
*
* Insert Time in SGT Here
*
*
*/

$startTime      = '2019-04-04 00:00:00';
$now            = '2019-04-07 23:30:00';
$nowTimeSlot    = explode(' ',$now);


// ================================================================================================================================================================
// =========================================================== DON'T EDIT ANYTHING FROM HERE AFTER ================================================================
// ================================================================================================================================================================


// Getting No of Days
$date1          = date_create($startTime);
$date2          = date_create($now);
$diff           = date_diff($date1 , $date2);
$NoOfDays       = $diff->format("%a")+1; // adding 1 to adjust it

// Getting TimeArray Slot
for($i=1; $i <= 48; $i++) {

    // Check Array
    if($GLOBALS['time_array'][$i] == $nowTimeSlot[1]) {
        $timeFrameId = $i;
    }

}

$start_day      = date('d',strtotime($startTime)); 
$start_month    = date('m',strtotime($startTime)); 
$start_year     = date('Y',strtotime($startTime)); 

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------

// Postgres Connection for AWS
$host             = "host   = pgsql-electrify-2.ckw3aym3r7qp.ap-southeast-1.rds.amazonaws.com";
$port             = "port   = 5432";
$credentials      = "user   = postgres password = fWkb5Ff22UHwVkwv";

$dbname           = "dbname = postgres";
$dbSynergy        = "dbname = Ecosystem";

// connecting to cms database
$db  = pg_connect("$host $port $dbname $credentials");

if(!$db) {
    echo "Error : Unable to open database\n";
}

// connecting to Synergy database
$dbs = pg_connect("$host $port $dbSynergy $credentials");

if(!$dbs) {
    echo "Error : Unable to open database\n";
}


// ----------------------------------------------------------------------------------------------------------------------------------------------------------------

function execStoreData($db,$dbs,$execTimeSlot,$PPId,$SlvId,$RegId) {

    $MeterType = 'C';

    $prevTimeSlot           = date('Y-m-d H:i:s', strtotime($execTimeSlot . "-30 minutes"));
    $execTimeSlotBuffer     = date('Y-m-d H:i:s', strtotime($execTimeSlot . "+2 minutes" ));
    
    
    // ------------------------------------------------------------------------------------------------------------------------------------------------------------


    $readingQry =   "
                            SELECT
                                t.pp_id,
                                to_timestamp(floor(date_part('epoch'::text, t.pp_date) / (30 * 60)::double precision) * (30 * 60)::double precision)::timestamp AS pp_date,
                                t.pp_readings,
                                t.pp_readings - lag(t.pp_readings, 1, t.pp_readings) OVER (PARTITION BY t.pp_id) AS adjusted_reading,             
                                t.slave_id,
                                t.register_id

                            FROM ( 
                                
                                SELECT

                                    tblreadings.pp_id,
                                    tblreadings.pp_date,
                                    first_value(tblreadings.pp_readings) OVER (PARTITION BY (to_timestamp(floor(date_part('epoch'::text, tblreadings.pp_date) / (30 * 60)::double precision) * (30 * 60)::double precision)::timestamp without time zone) ORDER BY tblreadings.pp_date) AS pp_readings,
                                    tblreadings.slave_id,
                                    tblreadings.register_id
                                
                                FROM 
                                    tblreadings
                                
                                WHERE 
                                    tblreadings.pp_id::text  = '".$PPId."'::text AND 
                                    tblreadings.slave_id     = '".$SlvId."' AND
                                    tblreadings.register_id  = '".$RegId."' AND
                                    tblreadings.pp_date BETWEEN '".$prevTimeSlot."'::timestamp without time zone AND '".$execTimeSlotBuffer."'::timestamp without time zone
                                
                                ORDER BY 
                                    tblreadings.pp_date
                                    
                                ) t

                            GROUP BY 
                                (to_timestamp(floor(date_part('epoch'::text, t.pp_date) / (30 * 60)::double precision) * (30 * 60)::double precision)::timestamp without time zone), 
                                t.pp_id, 
                                t.pp_readings, 
                                t.slave_id, 
                                t.register_id

                            ORDER BY 
                                (to_timestamp(floor(date_part('epoch'::text, t.pp_date) / (30 * 60)::double precision) * (30 * 60)::double precision)::timestamp without time zone);

                            ";
  
    // tapping readingQry
    $readingQryResult = pg_query($db, $readingQry);
    
    if(!$readingQryResult) {
        echo pg_last_error($db);
        exit;
    }


    // Checking number of records
    $numRecords =  pg_num_rows($readingQryResult);

    if($numRecords == 2) {

        // 2nd row
        $readingRow           = pg_fetch_array($readingQryResult, 1, PGSQL_ASSOC);
        $ReadingRowPPDate     = $readingRow['pp_date'];
        $ReadingRowAdjReading = $readingRow['adjusted_reading'];


        $AdjDateForUTC        = date('Y-m-d H:i:s', strtotime($ReadingRowPPDate . "-8 hours"));

        
        // Insert into PowerPod Readings
        $insertQry  =   '
                            INSERT INTO "PowerPod"."PowerPodReading" (
                                    "PPId", 
                                    "PPReading", 
                                    "PPDate", 
                                    "UserTypeRID", 
                                    "DateAdded", 
                                    "ReadingTypeRCode", 
                                    "SlaveRID", 
                                    "RegisterRID"
                            ) VALUES (
                                \''.$PPId.'\', 
                                \''.$ReadingRowAdjReading.'\', 
                                \''.$AdjDateForUTC.'\', 
                                    1, 
                                \''.date('Y-m-d H:i:s').'\', 
                                \''.$MeterType.'\', 
                                \''.$SlvId.'\', 
                                \''.$RegId.'\'
                            );
                        ';

            
            $insertRec = pg_query($dbs, $insertQry);
    
            if(!$insertRec) {
                echo pg_last_error($dbs);
            } 
            /**/

            // Insert into Log
            $LogQry     =   '
                                INSERT INTO "cms"."RefDerivedLog" (
                                    "PPId", 
                                    "SlaveId", 
                                    "RegisterId", 
                                    "LastUpdated"
                                ) VALUES (
                                    \''.$PPId.'\', 
                                    \''.$SlvId.'\', 
                                    \''.$RegId.'\', 
                                    \''.date('Y-m-d H:i:s').'\'
                                );
                            ';
            
            
            $logInsert = pg_query($db, $LogQry);
    
            if(!$logInsert) {
                echo pg_last_error($db);
            } else {
                echo "Added to the Log\n";
            }
           /* */
        
        
            // UPDATE "derived_updated" column
            $updateQry  =   '
                                UPDATE "cms"."tblpowerpodconfig" SET 
                                    derived_updated= \''.date('Y-m-d H:i:s').'\'
                                WHERE
                                    pp_id =  \''.$PPId.'\'
        
                            ';
            /*
            $Update = pg_query($db, $updateQry);
        
            if(!$Update) {
                echo pg_last_error($db);
            } else {
                echo "Record Updated successfully\n";
            }
            */




        // get first record
            // if first record value is null, 
            // then 2nd record - NULL = 0;

        // get second record


    } else {
        
        $AdjDateForUTC        = date('Y-m-d H:i:s', strtotime($execTimeSlot . "-8 hours"));

        // Insert into PowerPod Readings
        $insertQry  =   '
                            INSERT INTO "PowerPod"."PowerPodReading" (
                                    "PPId", 
                                    "PPReading", 
                                    "PPDate", 
                                    "UserTypeRID", 
                                    "DateAdded", 
                                    "ReadingTypeRCode", 
                                    "SlaveRID", 
                                    "RegisterRID"
                            ) VALUES (
                                \''.$PPId.'\', 
                                    NULL, 
                                \''.$AdjDateForUTC.'\', 
                                    1, 
                                \''.date('Y-m-d H:i:s').'\', 
                                \''.$MeterType.'\', 
                                \''.$SlvId.'\', 
                                \''.$RegId.'\'
                            );
                        ';


        //echo $insertQry;
            
        $insertRec = pg_query($dbs, $insertQry);

        if(!$insertRec) {
            echo pg_last_error($dbs);
        } 
        /* */

        // Insert into Log
        $LogQry     =   '
                            INSERT INTO "cms"."RefDerivedLog" (
                                "PPId", 
                                "SlaveId", 
                                "RegisterId", 
                                "LastUpdated"
                            ) VALUES (
                                \''.$PPId.'\', 
                                \''.$SlvId.'\', 
                                \''.$RegId.'\', 
                                \''.date('Y-m-d H:i:s').'\'
                            );
                        ';

        
        $logInsert = pg_query($db, $LogQry);

        if(!$logInsert) {
        echo pg_last_error($db);
        } else {
        echo "Added to the Log\n";
        }
        /*  */


        // UPDATE "derived_updated" column
        $updateQry  =   '
                            UPDATE "cms"."tblpowerpodconfig" SET 
                                derived_updated= \''.date('Y-m-d H:i:s').'\'
                            WHERE
                                pp_id =  \''.$PPId.'\'

                        ';
        /*
        $Update = pg_query($db, $updateQry);

        if(!$Update) {
        echo pg_last_error($db);
        } else {
        echo "Record Updated successfully\n";
        }
        */


    }



}


// ----------------------------------------------------------------------------------------------------------------------------------------------------------------

/* Main Query */
$mainQry =  "
                SELECT 
                    cms.tblsensorconfig.pp_id,
                    cms.tblpowerpodconfig.belongs_to,
                    cms.tblpowerpodconfig.meter_type,
                    cms.tblsensorconfig.slave_id,
                    cms.tblsensorconfig.register_id,
                    cms.tblsensorconfig.register_description,
                    cms.tblsensorconfig.register_type,
                    cms.tblsensorconfig.accurate_as_at,
                    cms.tblpowerpodconfig.derived_updated

                FROM
                    cms.tblsensorconfig

                LEFT JOIN 
                    cms.tblpowerpodconfig ON cms.tblpowerpodconfig.pp_id = cms.tblsensorconfig.pp_id

                WHERE
                    cms.tblsensorconfig.pp_id IN (

                        SELECT 
                            pp_id
                        FROM
                            cms.tblpowerpodconfig
                        WHERE  
                            pp_id = '16edeb1b65d45835af22662d4f5b8f910adb3f2d6e214d05b8632f163e9ea99a'
                        ORDER BY
                            belongs_to

                ) AND

                    cms.tblsensorconfig.slave_id = cms.tblpowerpodconfig.cons_slave_id AND
                    cms.tblsensorconfig.register_id >= 0 AND
                    cms.tblsensorconfig.register_type IN ('EA')

                ORDER BY
                    belongs_to

            ";


// tapping mainQry
$mainQryRet = pg_query($db, $mainQry);
if(!$mainQryRet) {
   echo pg_last_error($db);
   exit;
}

// looping mainQry
while($row = pg_fetch_array($mainQryRet)) {

    $mainQryPPId           = $row['pp_id'];
    $mainQryBelongsTo      = $row['belongs_to'];

    $mainQrySlvId          = $row['slave_id'];
    $mainQryRegId          = $row['register_id'];
    $mainQryRegType        = $row['register_type'];
    $mainQryAccurate       = $row['accurate_as_at'];

    $mainQryMeterType      = $row['meter_type'];
    $mainQryDerivedUpdated = $row['derived_updated'];


    // Loop through Days
    for($j=1; $j <= $NoOfDays; $j++) {

        // adding leading zero
        $PropDate = sprintf("%02d", $start_day);

        if($now === date("$start_year-$start_month-$PropDate $time_array[$timeFrameId]")) {
            
            // Loop for Particular Period Only
            for($x=1; $x <= $timeFrameId; $x++) {
                
                $exactTime = date("$start_year-$start_month-$PropDate").' '.$time_array[$x];
                execStoreData($db,$dbs,$exactTime,$mainQryPPId,$mainQrySlvId,$mainQryRegId);
                //echo $exactTime."<br />";

            }
        
        } else {

            // loop for 48 period
            for($k=1; $k <= 48; $k++) {
                
                $exactTime = date("$start_year-$start_month-$PropDate").' '.$time_array[$k];
                execStoreData($db,$dbs,$exactTime,$mainQryPPId,$mainQrySlvId,$mainQryRegId);

                //echo $exactTime."<br />";

            }

        }

        $start_day = $start_day + 1;

    }

}



pg_close($db);
pg_close($dbs);

