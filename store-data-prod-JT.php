<?php

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------

// time is saved by GMT 0
date_default_timezone_set('UTC');

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------

// a53fc2c7a5444f12ae3d0908ea710942398f246672187ee62e05d2b56611b5c1
// 45186               - Pure Production
// (45186 - 45188) + 45184   - Pure Consumption
// (45184 + 45188)        - Nett Import / Export ( [+] Import ~ [-] Export )

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------

/*
*
*
* Insert Time in SGT Here
*
*
*/

$startTime      = '2019-04-01 00:00:00';
$now            = '2019-04-08 23:30:00';

$PPId           = 'a53fc2c7a5444f12ae3d0908ea710942398f246672187ee62e05d2b56611b5c1';
$UserTypeRID    = 3;

// ================================================================================================================================================================
// =========================================================== DON'T EDIT ANYTHING FROM HERE AFTER ================================================================
// ================================================================================================================================================================

// Postgres Connection for AWS
$host             = "host   = pgsql-electrify-2.ckw3aym3r7qp.ap-southeast-1.rds.amazonaws.com";
$port             = "port   = 5432";
$credentials      = "user   = postgres password = fWkb5Ff22UHwVkwv";

$dbname           = "dbname = postgres";
$dbSynergy        = "dbname = Ecosystem";

// connecting to cms database
$db  = pg_connect("$host $port $dbname $credentials");
if(!$db) {
    echo "Error : Unable to open database\n";
}

// connecting to Synergy database
$dbs = pg_connect("$host $port $dbSynergy $credentials");
if(!$dbs) {
    echo "Error : Unable to open database\n";
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------


// Time Array
$GLOBALS['time_array'][1]  ='00:00:00';
$GLOBALS['time_array'][2]  ='00:30:00';
$GLOBALS['time_array'][3]  ='01:00:00';
$GLOBALS['time_array'][4]  ='01:30:00';
$GLOBALS['time_array'][5]  ='02:00:00';
$GLOBALS['time_array'][6]  ='02:30:00';
$GLOBALS['time_array'][7]  ='03:00:00';
$GLOBALS['time_array'][8]  ='03:30:00';
$GLOBALS['time_array'][9]  ='04:00:00';
$GLOBALS['time_array'][10] ='04:30:00';
$GLOBALS['time_array'][11] ='05:00:00';
$GLOBALS['time_array'][12] ='05:30:00';
$GLOBALS['time_array'][13] ='06:00:00';
$GLOBALS['time_array'][14] ='06:30:00';
$GLOBALS['time_array'][15] ='07:00:00';
$GLOBALS['time_array'][16] ='07:30:00';
$GLOBALS['time_array'][17] ='08:00:00';
$GLOBALS['time_array'][18] ='08:30:00';
$GLOBALS['time_array'][19] ='09:00:00';
$GLOBALS['time_array'][20] ='09:30:00';
$GLOBALS['time_array'][21] ='10:00:00';
$GLOBALS['time_array'][22] ='10:30:00';
$GLOBALS['time_array'][23] ='11:00:00';
$GLOBALS['time_array'][24] ='11:30:00';
$GLOBALS['time_array'][25] ='12:00:00';
$GLOBALS['time_array'][26] ='12:30:00';
$GLOBALS['time_array'][27] ='13:00:00';
$GLOBALS['time_array'][28] ='13:30:00';
$GLOBALS['time_array'][29] ='14:00:00';
$GLOBALS['time_array'][30] ='14:30:00';
$GLOBALS['time_array'][31] ='15:00:00';
$GLOBALS['time_array'][32] ='15:30:00';
$GLOBALS['time_array'][33] ='16:00:00';
$GLOBALS['time_array'][34] ='16:30:00';
$GLOBALS['time_array'][35] ='17:00:00';
$GLOBALS['time_array'][36] ='17:30:00';
$GLOBALS['time_array'][37] ='18:00:00';
$GLOBALS['time_array'][38] ='18:30:00';
$GLOBALS['time_array'][39] ='19:00:00';
$GLOBALS['time_array'][40] ='19:30:00';
$GLOBALS['time_array'][41] ='20:00:00';
$GLOBALS['time_array'][42] ='20:30:00';
$GLOBALS['time_array'][43] ='21:00:00';
$GLOBALS['time_array'][44] ='21:30:00';
$GLOBALS['time_array'][45] ='22:00:00';
$GLOBALS['time_array'][46] ='22:30:00';
$GLOBALS['time_array'][47] ='23:00:00';
$GLOBALS['time_array'][48] ='23:30:00';


// ----------------------------------------------------------------------------------------------------------------------------------------------------------------


// time manipulation based on the startdate and end date above

$nowTimeSlot = explode(' ',$now);

// Getting No of Days
$date1       = date_create($startTime);
$date2       = date_create($now);
$diff        = date_diff($date1 , $date2);
$NoOfDays    = $diff->format("%a")+1; // adding 1 to adjust it

// Getting TimeArray Slot
for($i=1; $i <= 48; $i++) {
    // Check Array
    if($GLOBALS['time_array'][$i] == $nowTimeSlot[1]) {
        $timeFrameId = $i;
    }
}

$start_day = date('d',strtotime($startTime)); 
$start_month = date('m',strtotime($startTime)); 


// ----------------------------------------------------------------------------------------------------------------------------------------------------------------


// Get Slave Id(s)

$RegIdList  =   '45184,45186,45188';

$SlvQry     =   "
                    SELECT 
                        cms.tblsensorconfig.pp_id,
                        cms.tblpowerpodconfig.belongs_to,
                        cms.tblpowerpodconfig.meter_type,
                        cms.tblsensorconfig.slave_id,
                        cms.tblsensorconfig.register_id,
                        cms.tblsensorconfig.register_description,
                        cms.tblsensorconfig.register_type,
                        cms.tblsensorconfig.accurate_as_at,
                        cms.tblpowerpodconfig.derived_updated

                    FROM
                        cms.tblsensorconfig

                    LEFT JOIN 
                        cms.tblpowerpodconfig ON cms.tblpowerpodconfig.pp_id = cms.tblsensorconfig.pp_id

                    WHERE
                        cms.tblsensorconfig.pp_id = '".$PPId."' AND
                        cms.tblsensorconfig.register_id IN (".$RegIdList.")

                    ORDER BY
                        cms.tblsensorconfig.register_id

                    ";

$SlvQryRes  = pg_query($db, $SlvQry);

if(!$SlvQryRes) {
    echo pg_last_error($db);
    exit;
}

while($row = pg_fetch_array($SlvQryRes)) {

    $SlvIdArr[$row['register_id']] = $row['slave_id'];

}

// -----


function FetchRegData($db,$execTimeSlot,$PPId,$RegId) {

    // Set Date
    $EffPrvSlot        = date('Y-m-d H:i:s', strtotime($execTimeSlot . "-30 minutes"));
    $EffSlot           = date('Y-m-d H:i:s', strtotime($execTimeSlot . "+2 minutes" ));

    $readingQry =   "
                        SELECT
                            to_timestamp(floor(date_part('epoch'::text, t.pp_date) / (30 * 60)::double precision) * (30 * 60)::double precision)::timestamp AS pp_date,
                            t.pp_readings,
                            t.pp_readings - lag(t.pp_readings, 1, t.pp_readings) OVER (PARTITION BY t.pp_id) AS adjusted_reading,             
                            t.slave_id,
                            t.register_id

                        FROM ( 
                            
                            SELECT

                                tblreadings.pp_id,
                                tblreadings.pp_date,
                                first_value(tblreadings.pp_readings) OVER (PARTITION BY (to_timestamp(floor(date_part('epoch'::text, tblreadings.pp_date) / (30 * 60)::double precision) * (30 * 60)::double precision)::timestamp without time zone) ORDER BY tblreadings.pp_date) AS pp_readings,
                                tblreadings.slave_id,
                                tblreadings.register_id
                            
                            FROM 
                                tblreadings
                            
                            WHERE 
                                tblreadings.pp_id::text     = '".$PPId."'::text AND 
                                tblreadings.register_id     = '".$RegId."' AND
                                tblreadings.pp_date BETWEEN '".$EffPrvSlot."'::timestamp without time zone AND '".$EffSlot."'::timestamp without time zone
                            
                            ORDER BY 
                                tblreadings.pp_date
                                
                            ) t

                        GROUP BY 
                            (to_timestamp(floor(date_part('epoch'::text, t.pp_date) / (30 * 60)::double precision) * (30 * 60)::double precision)::timestamp without time zone), 
                            t.pp_id, 
                            t.pp_readings, 
                            t.slave_id, 
                            t.register_id

                        ORDER BY 
                            (to_timestamp(floor(date_part('epoch'::text, t.pp_date) / (30 * 60)::double precision) * (30 * 60)::double precision)::timestamp without time zone);

                    ";

    $readingQryResult = pg_query($db, $readingQry);

    if(!$readingQryResult) {
        echo pg_last_error($db);
        exit;
    }

    // Checking number of records
    $numRecords =  pg_num_rows($readingQryResult);

    // And exactly for two records (Current Record and Previous Record)
    if($numRecords == 2) {
        
        $readingRow  = pg_fetch_array($readingQryResult, 1, PGSQL_ASSOC);
        return $readingRow;

    } else {

        return array('adjusted_reading' => NULL);

    }

}


function InsertCalculatedValue($db,$dbs,$PPDate,$PPId,$UserTypeRID,$SlaveId,$RegId,$Value,$Func) {

    $PPDateUTC        = date('Y-m-d H:i:s', strtotime($PPDate . "-8 hours"));
            
    if($Value === NULL) {

        $insertQry  =   '
                            INSERT INTO "PowerPod"."PowerPodReading" (
                                    "PPId", 
                                    "PPReading", 
                                    "PPDate", 
                                    "UserTypeRID", 
                                    "DateAdded", 
                                    "ReadingTypeRCode", 
                                    "SlaveRID", 
                                    "RegisterRID"
                            ) VALUES (
                                \''.$PPId.'\', 
                                    NULL,
                                \''.$PPDateUTC.'\', 
                                \''.$UserTypeRID.'\', 
                                \''.date('Y-m-d H:i:s').'\', 
                                \''.$Func.'\', 
                                \''.$SlaveId.'\', 
                                \''.$RegId.'\'
                            );
                        ';

    } else {

        $insertQry  =   '
                            INSERT INTO "PowerPod"."PowerPodReading" (
                                    "PPId", 
                                    "PPReading", 
                                    "PPDate", 
                                    "UserTypeRID", 
                                    "DateAdded", 
                                    "ReadingTypeRCode", 
                                    "SlaveRID", 
                                    "RegisterRID"
                            ) VALUES (
                                \''.$PPId.'\', 
                                \''.$Value.'\', 
                                \''.$PPDateUTC.'\', 
                                \''.$UserTypeRID.'\', 
                                \''.date('Y-m-d H:i:s').'\', 
                                \''.$Func.'\', 
                                \''.$SlaveId.'\', 
                                \''.$RegId.'\'
                            );
                        ';

    }


    $insertRec = pg_query($dbs, $insertQry);

    if(!$insertRec) {
        echo pg_last_error($dbs);
    } 
    /* */

    // UPDATE "derived_updated" column
    $updateQry  =   '
                        UPDATE "cms"."tblpowerpodconfig" SET 
                            derived_updated= \''.date('Y-m-d H:i:s', strtotime('+8 hours')).'\'
                        WHERE
                            pp_id =  \''.$PPId.'\'
                    ';
    
    $Update = pg_query($db, $updateQry);

    if(!$Update) {
        echo pg_last_error($db);
    } 


}


function StoreData($db,$dbs,$execTime,$PPId,$SlvIdArr,$UserTypeRID) {


    // --------------------------------------------------------

    $NIRegId     = 45184;
    $NettImport  = FetchRegData($db,$execTime,$PPId,$NIRegId);

    $NERegId     = 45188;
    $NettExport  = FetchRegData($db,$execTime,$PPId,$NERegId);

    $PRegId      = 45186;
    $PureProd    = FetchRegData($db,$execTime,$PPId,$PRegId);

    if(
        (is_null($NettImport['adjusted_reading']) == 1) &&
        (is_null($NettExport['adjusted_reading']) == 1) &&
        (is_null($PureProd['adjusted_reading']) == 1)
    ) {

        // ------------------------------------------------------ Store Pure Production ------------------------------------------------------

        InsertCalculatedValue($db,$dbs,$execTime,$PPId,$UserTypeRID,$SlvIdArr[$PRegId],$PRegId,NULL,'P');

        // ------------------------------------------------------ Store Pure Consumption -----------------------------------------------------

        InsertCalculatedValue($db,$dbs,$execTime,$PPId,$UserTypeRID,1,98,NULL,'C');

        // ------------------------------------------------------ Store Nett Import ----------------------------------------------------------

        InsertCalculatedValue($db,$dbs,$execTime,$PPId,$UserTypeRID,$SlvIdArr[$NIRegId],$NIRegId,NULL,'I');

        // ------------------------------------------------------ Store Nett Export ----------------------------------------------------------

        InsertCalculatedValue($db,$dbs,$execTime,$PPId,$UserTypeRID,$SlvIdArr[$NERegId],$NERegId,NULL,'E');

        // ------------------------------------------------------ Store Nett Import/ Export --------------------------------------------------

        InsertCalculatedValue($db,$dbs,$execTime,$PPId,$UserTypeRID,1,99,NULL,'N');

        // -----------------------------------------------------------------------------------------------------------------------------------

    } else {


        if(is_null($NettImport['adjusted_reading']) == 1) { $NettImportReading = 0; } else { $NettImportReading = $NettImport['adjusted_reading']; }
        if(is_null($NettExport['adjusted_reading']) == 1) { $NettExportReading = 0; } else { $NettExportReading = $NettExport['adjusted_reading']; }
        if(is_null($PureProd['adjusted_reading']) == 1)   { $PureProdReading   = 0; } else { $PureProdReading   = $PureProd['adjusted_reading'];   }


        // ------------------------------------------------------ Store Pure Production ------------------------------------------------------

        InsertCalculatedValue($db,$dbs,$execTime,$PPId,$UserTypeRID,$SlvIdArr[$PRegId],$PRegId,$PureProdReading,'P');

        // ------------------------------------------------------ Store Pure Consumption -----------------------------------------------------

        $PCValue = ($PureProdReading - $NettExportReading ) + $NettImportReading;
        InsertCalculatedValue($db,$dbs,$execTime,$PPId,$UserTypeRID,1,98,$PCValue,'C');

        // ------------------------------------------------------ Store Nett Import ----------------------------------------------------------

        InsertCalculatedValue($db,$dbs,$execTime,$PPId,$UserTypeRID,$SlvIdArr[$NIRegId],$NIRegId,$NettImportReading,'I');

        // ------------------------------------------------------ Store Nett Export ----------------------------------------------------------

        $NettExportReadingABS = -(abs($NettExportReading));
        InsertCalculatedValue($db,$dbs,$execTime,$PPId,$UserTypeRID,$SlvIdArr[$NERegId],$NERegId,$NettExportReadingABS,'E');

        // ------------------------------------------------------ Store Nett Import/ Export --------------------------------------------------

        $NettImportExport = $NettImportReading - $NettExportReading;
        InsertCalculatedValue($db,$dbs,$execTime,$PPId,$UserTypeRID,1,99,$NettImportExport,'N');

        // -----------------------------------------------------------------------------------------------------------------------------------


    }


}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------




// Loop through Days

echo "Started";

for($j=1; $j <= $NoOfDays; $j++) {

    // adding leading zero
    $PropDate = sprintf("%02d", $start_day);

    if($now === date("Y-$start_month-$PropDate $time_array[$timeFrameId]")) {
        
        // Loop for Particular Period Only
        for($x=1; $x <= $timeFrameId; $x++) {
            
            $execTime    = date("Y-$start_month-$PropDate").' '.$time_array[$x];
            StoreData($db,$dbs,$execTime,$PPId,$SlvIdArr,$UserTypeRID);

        }
    
    } else {

        // loop for 48 period
        for($k=1; $k <= 48; $k++) {
            
            $execTime = date("Y-$start_month-$PropDate").' '.$time_array[$k];
            StoreData($db,$dbs,$execTime,$PPId,$SlvIdArr,$UserTypeRID);

        }

    }

    $start_day = $start_day + 1;

}


echo "Done";