# Populate Records / Insert Missing Records

written by AJ.
Follow the time settings in every script. Mentioned whether you have to put time in SGT or UTC.


### Electrfy Demo (Evergreen)
store-data-demo-xxxxxxx.php


### Electrfy Production
store-data-prod-xxxxxxx.php

In case, marketdata is missing in production, SOS me. :)
there's already a cron job running for check marketdata in production server. 

So, technology@electrify.sg will receive an email if there's missing records.